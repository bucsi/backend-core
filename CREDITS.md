Credits
==================

Project Members
------------------

| Name            | Tasks        |
| --------------- | ------------ |
| Zsolt Bögre     | Task analytics, RBAC |
| Máté Cserép     | Project lead, architect, CI |
| Éva Hartung     | Version controlled submissions (Git integration) |
| Tamás J. Tóth   | User settings, Plagiarism check (basefiles, Moss persistence) |
| Kristóf Károlyi | Plagiarism check (MOSS persistence) |
| Péter Kaszab    | React-based frontend, REST API |
| Kálmán Kostenszky | Remote execution for web applications, Docker integration |
| Balázs Kübler   | Administrator panel, RBAC |
| Dávid Nagy      | Canvas integration and synchronization |
| Gergő Prepok    | Initial core functionalities, plagiarism check (MOSS integration) |
| Krisztián Szögi | Multiple-choice examination |
| Márk Várszegi   | CodeCompass integration |
| Zoltán Zele     | Automated evaluator, Docker integration |
