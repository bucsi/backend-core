<?php

return [
    'subscription1' => [
        'id' => 1,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2000,
        'semesterID' => 3001
    ],
    'subscription2' => [
        'id' => 2,
        'userID' => 1002,
        'isAccepted' => 1,
        'groupID' => 2000,
        'semesterID' => 3001
    ],
    'subscription3' => [
        'id' => 3,
        'userID' => 1003,
        'isAccepted' => 1,
        'groupID' => 2000,
        'semesterID' => 3001
    ],
    'subscription4' => [
        'id' => 4,
        'userID' => 1002,
        'isAccepted' => 1,
        'groupID' => 2007,
        'semesterID' => 3001
    ],
    'subscription5' => [
        'id' => 5,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2001,
        'semesterID' => 3001
    ],
    'subscription6' => [
        'id' => 6,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2005,
        'semesterID' => 3001
    ],
    'subscription7' => [
        'id' => 7,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2010,
        'semesterID' => 3000
    ],
    'subscription8' => [
        'id' => 8,
        'userID' => 4,
        'isAccepted' => 0,
        'groupID' => 11,
        'semesterID' => 1
    ]
];
