<?php

return [
    'basefile1' => [
        'id' => 6000,
        'name' => 'DelegateCommand.cs',
        'lastUpdateTime' => date('Y/m/d H:i:s'),
        'courseID' => 4000,
        'uploaderID' => 1007,
    ],
    'basefile2' => [
        'id' => 6001,
        'name' => 'catch.hpp',
        'lastUpdateTime' => date('Y/m/d h:i:s'),
        'courseID' => 4003,
        'uploaderID' => 1000,
    ],
];
