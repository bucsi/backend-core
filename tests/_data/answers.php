<?php

return [
    "answer1" => [
        "id" => 1,
        "text" => "Answer 1",
        "correct" => 1,
        "questionID" => 1,
    ],
    "answer2" => [
        "id" => 2,
        "text" => "Answer 2",
        "correct" => 0,
        "questionID" => 1,
    ],
    "answer3" => [
        "id" => 3,
        "text" => "Answer 3",
        "correct" => 0,
        "questionID" => 1,
    ],
    "answer4" => [
        "id" => 4,
        "text" => "Answer 4",
        "correct" => 1,
        "questionID" => 1,
    ],
    "answer5" => [
        "id" => 5,
        "text" => "Answer 5",
        "correct" => 1,
        "questionID" => 1,
    ],


    "answer6" => [
        "id" => 6,
        "text" => "Answer 1",
        "correct" => 1,
        "questionID" => 2,
    ],
    "answer7" => [
        "id" => 7,
        "text" => "Answer 2",
        "correct" => 0,
        "questionID" => 2,
    ],
    "answer8" => [
        "id" => 8,
        "text" => "Answer 3",
        "correct" => 0,
        "questionID" => 2,
    ],
    "answer9" => [
        "id" => 9,
        "text" => "Answer 4",
        "correct" => 1,
        "questionID" => 2,
    ],


    "answer10" => [
        "id" => 10,
        "text" => "Answer 1",
        "correct" => 1,
        "questionID" => 3,
    ],
    "answer11" => [
        "id" => 11,
        "text" => "Answer 2",
        "correct" => 0,
        "questionID" => 3,
    ],
    "answer12" => [
        "id" => 12,
        "text" => "Answer 3",
        "correct" => 0,
        "questionID" => 3,
    ],
    "answer13" => [
        "id" => 13,
        "text" => "Answer 4",
        "correct" => 1,
        "questionID" => 3,
    ],

    "answer14" => [
        "id" => 14,
        "text" => "Answer 1",
        "correct" => 1,
        "questionID" => 5,
    ],

    "answer15" => [
        "id" => 15,
        "text" => "Answer 1",
        "correct" => 1,
        "questionID" => 6,
    ]
];
