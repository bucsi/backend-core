<?php

return [
    'instructorgroup1' => [
        'userID' => 1007,
        'groupID' => 2000,
    ],
    'instructorgroup2' => [
        'userID' => 1007,
        'groupID' => 2006,
    ],
    'instructorgroup3' => [
        'userID' => 1007,
        'groupID' => 2010,
    ],
    'instructorgroup4' => [
        'userID' => 1006,
        'groupID' => 2001,
    ],
    'instructorgroup5' => [
        'userID' => 1006,
        'groupID' => 2002,
    ],
    'instructorgroup6' => [
        'userID' => 1008,
        'groupID' => 2006,
    ],
    'instructorgroup7' => [
        'userID' => 1009,
        'groupID' => 2006,
    ],
    'instructorgroup8' => [
        'userID' => 1009,
        'groupID' => 2007,
    ],
    'instructorgroup9' => [
        'userID' => 1007,
        'groupID' => 2005,
    ],
    'instructorgroup10' => [
        'userID' => 1009,
        'groupID' => 2005,
    ],
    'instructorgroup11' => [
        'userID' => 1008,
        'groupID' => 2004,
    ],
    'instructorgroup12' => [
        'userID' => 1009,
        'groupID' => 2004,
    ],
];
