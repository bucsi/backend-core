<?php

return [
    'testcase1' => [
        'id' => 1,
        'input' => '1',
        'output' => '1',
        'arguments' => '1',
        'taskID' => 5000
    ],
    'testcase2' => [
        'id' => 2,
        'input' => '2',
        'output' => '4',
        'arguments' => '8',
        'taskID' => 5000
    ],
    'testcase3' => [
        'id' => 3,
        'input' => '3',
        'output' => '9',
        'arguments' => '27',
        'taskID' => 5004
    ],
    'testcase4' => [
        'id' => 4,
        'input' => '3',
        'output' => '9',
        'arguments' => '27',
        'taskID' => 5005
    ]
];
