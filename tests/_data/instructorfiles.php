<?php

return [
    'file1' => [
        'id' => 1,
        'name' => 'file1.txt',
        'uploadTime' => '2021-02-01 10:00:00',
        'taskID' => 5000,
        'category' => 'Attachment'
    ],
    'file2' => [
        'id' => 2,
        'name' => 'file2.txt',
        'uploadTime' => '2021-02-02 10:00:00',
        'taskID' => 5000,
        'category' => 'Attachment'
    ],
    'file3' => [
        'id' => 3,
        'name' => 'file3.txt',
        'uploadTime' => '2021-02-03 10:00:00',
        'taskID' => 5000,
        'category' => 'Attachment'
    ],
    'file4' => [
        'id' => 4,
        'name' => 'file1.txt',
        'uploadTime' => '2021-02-03 10:00:00',
        'taskID' => 5003,
        'category' => 'Attachment'
    ],
    'file5' => [
        'id' => 5,
        'name' => 'file1.txt',
        'uploadTime' => '2021-02-03 10:00:00',
        'taskID' => 5004,
        'category' => 'Attachment'
    ],
    'file6' => [
        'id' => 6,
        'name' => 'file1.txt',
        'uploadTime' => '2021-02-03 10:00:00',
        'taskID' => 5005,
        'category' => 'Attachment'
    ],
    'file7' => [
        'id' => 7,
        'name' => 'file1.txt',
        'uploadTime' => '2021-02-03 10:00:00',
        'taskID' => 5007,
        'category' => 'Attachment'
    ],
    'file8' => [
        'id' => 8,
        'name' => 'file2.txt',
        'uploadTime' => '2021-02-04 10:00:00',
        'taskID' => 5003,
        'category' => 'Test file'
    ],
];
