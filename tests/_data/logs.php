<?php

return [
    'file1_upload_log1' => [
        'id' => 1,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 2 (5001)',
    ],
    'file2_upload_log1' => [
        'id' => 2,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student Two (stud02)]',
        'message' => 'A new solution has been uploaded for Task 2 (5001)',
    ],
    'file3_upload_log1' => [
        'id' => 3,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 3 (5002)',
    ],
    'file4_upload_log1' => [
        'id' => 4,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student Two (stud02)]',
        'message' => 'A new solution has been uploaded for Task 3 (5002)',
    ],
    'file5_upload_log1' => [
        'id' => 5,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student Two (stud02)]',
        'message' => 'A new solution has been uploaded for Task 8 (5007)',
    ],
    'file6_upload_log1' => [
        'id' => 6,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 5 (5004)',
    ],
    'file7_upload_log1' => [
        'id' => 7,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 6 (5005)',
    ],
    'file8_upload_log1' => [
        'id' => 8,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 2 (5001)',
    ],
    'file9_upload_log1' => [
        'id' => 9,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 10 (5009)',
    ],
    'file10_upload_log1' => [
        'id' => 11,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student Two (stud02)]',
        'message' => 'A new solution has been uploaded for Task 10 (5009)',
    ],
    'file11_upload_log1' => [
        'id' => 12,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student Three (stud03)]',
        'message' => 'A new solution has been uploaded for Task 10 (5009)',
    ],
    'file12_upload_log1' => [
        'id' => 13,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 12 (5011)',
    ],
    'file13_upload_log1' => [
        'id' => 14,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 13 (5012)',
    ],
    'file14_upload_log1' => [
        'id' => 15,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student Two (stud02)]',
        'message' => 'A new solution has been uploaded for Task 12 (5011)',
    ],
    'file15_upload_log1' => [
        'id' => 16,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.2][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 14 (5013)',
    ],
    'file16_upload_log1' => [
        'id' => 17,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-5 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 15 (5014)',
    ],
    'file16_upload_log2' => [
        'id' => 18,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-4 minute'))),
        'prefix' => '[192.168.1.2][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 15 (5014)',
    ],
    'file16_upload_log3' => [
        'id' => 19,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-3 minute'))),
        'prefix' => '[192.168.1.2][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 15 (5014)',
    ],
    'file16_upload_log4' => [
        'id' => 20,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-2 minute'))),
        'prefix' => '[192.168.1.1][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 15 (5014)',
    ],
    'file16_upload_log5' => [
        'id' => 21,
        'level' => 4,
        'category' => 'app\modules\student\controllers\StudentFilesController::saveFile',
        'log_time' => strtotime(date('Y-m-d H:i:s', strtotime('-1 minute'))),
        'prefix' => '[192.168.1.3][Student One (stud01)]',
        'message' => 'A new solution has been uploaded for Task 15 (5014)',
    ],
];
