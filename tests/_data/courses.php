<?php

return [
    'course0' => [
        'id' => 4000,
        'name' => 'Java',
        'code' => 1,
    ],
    'course1' => [
        'id' => 4001,
        'name' => 'C++',
        'code' => 2,
    ],
    'course3' => [
        'id' => 4002,
        'name' => 'C#',
        'code' => 3,
    ],
    'course4' => [
        'id' => 4003,
        'name' => 'OEP',
        'code' => 4,
    ],
];
