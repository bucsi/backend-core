<?php

namespace app\modules\instructor\resources;

use app\components\openapi\generators\OAItems;
use app\components\openapi\generators\OAProperty;
use app\models\ExamQuestionSet;
use app\resources\CourseResource;
use yii\helpers\ArrayHelper;

class ExamQuestionSetResource extends ExamQuestionSet
{
    public function fields()
    {
        return [
            'id',
            'name',
            'course',
            'courseID'
        ];
    }

    public function extraFields()
    {
        return [
            'tests'
        ];
    }

    public function fieldTypes(): array
    {
        return ArrayHelper::merge(
            parent::fieldTypes(),
            [
                'course' => new OAProperty(['ref' => '#/components/schemas/Common_CourseResource_Read']),
                'tests' => new OAProperty(
                    [
                        'type' => 'array',
                        new OAItems(['ref' => '#/components/schemas/Instructor_ExamTestResource_Read'])
                    ]
                ),
            ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(CourseResource::class, ['id' => 'courseID']);
    }

    public function getTests()
    {
        return $this->hasMany(ExamTestResource::class, ['questionsetID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(ExamQuestionResource::class, ['questionsetID' => 'id']);
    }
}
