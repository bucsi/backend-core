<?php

/**
 * @OA\Schema(
 *   schema="int_id_list",
 *   type="string",
 *   format="([1-9][0-9]*(,[1-9][0-9]*)*)?"
 * )
 */
