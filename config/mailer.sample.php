<?php

return [
    'class' => 'yii\swiftmailer\Mailer',
    // Comment this to enable real mail transport
    'useFileTransport' => true,
    // Uncomment and configure this for SMTP mail transport
    // (By default sendmail transport is utilized.)
    //'transport' => [
    //    'class' => 'Swift_SmtpTransport',
    //    'host' => '',
    //    'username' => '',
    //    'password' => '',
    //    'port' => '',
    //    'encryption' => 'tls',
    //],
];
